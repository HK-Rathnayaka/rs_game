﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class Home : MonoBehaviour
{

    // Declare variables

    [SerializeField]
    private float time = 5f;

    [SerializeField]
    private string sceneName;

    private float timeTaken;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeTaken += Time.deltaTime;

        if (timeTaken > time) {
            SceneManager.LoadScene(sceneName);
        }
    }
}
