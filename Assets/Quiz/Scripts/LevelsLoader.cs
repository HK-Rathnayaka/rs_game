﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LevelsLoader : MonoBehaviour
{
    
    
    public void LevelsLoad(int level)
    {
        SceneManager.LoadScene(level);
    }
}
